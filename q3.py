import math
from q1 import Point3D
import sys
import decimal

def genSphere(rayon, m, p):
    u = 2*math.pi/m
    v = math.pi/p


    x = lambda i, j : rayon * math.cos(u * i) * math.sin(v * j)
    y = lambda i, j : rayon * math.sin(u * i) * math.sin(v * j)
    z = lambda j : rayon * math.cos(u * j)
    
    """ Sommets """
    for i in range(m):
        for j in range(p + 1):
            print("v {0} {1} {2}".format(round(x(i,j),12),round(y(i,j),12),round(z(j),12))) 

    """Vecteurs normaux """
    for i in range(m):
        for j in range(p + 1):
            print("vn {0} {1} {2}".format(round(-x(i,j)/rayon,12),round(-y(i,j)/rayon,12) ,round(-z(j)/rayon,12)))

    """ Faces """
    for j in range(1,((m + 1) * p )):
        if (j%(p + 1) == 0): pass
        else :
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, j + 2 + p, j + 1))
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, j + 1 + p, j + 2 + p))
    i = 0
    for j in range((m + 1) * p,  m * (p + 1)):
        if (j%(p + 1) == 0): pass
        else:
            i+=1
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, i + 1, j + 1))
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, i , i + 1))


def genTor(rMajeur, rMineur, m, p):
    u = 2*math.pi/m
    v = 2*math.pi/p
    x = lambda i, j : (rMajeur + rMineur * math.cos(u * i)) * math.cos(v * j)
    y = lambda i, j : (rMajeur + rMineur * math.cos(u * i)) * math.sin(v * j)
    z = lambda j : rMineur * math.sin(u * i)
    
    """ Sommets """
    for i in range(m):
        for j in range(p + 1):
            print("v {0} {1} {2}".format(round(x(i,j),12),round(y(i,j)),round(z(j))))

    """Vecteurs normaux """
    for i in range(m):
        for j in range(p + 1):
            print("vn {0} {1} {2}".format(round(math.sin(u * i),12), round(math.cos(u * i)*math.sin(v * j),12), round(math.cos(u * i)*math.sin(v * j),12)))

    """ Faces """
    for j in range(1,((m + 1) * p )):
        if (j%(p + 1) == 0): pass
        else :
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, j + 2 + p, j + 1))
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, j + 1 + p, j + 2 + p))
    i = 0
    for j in range((m + 1) * p,  m * (p + 1)):
        if (j%(p + 1) == 0): pass
        else:
            i+=1
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, i + 1, j + 1))
            print("f {0}//{0} {1}//{1} {2}//{2}".format(j, i , i + 1))


if(sys.argv[1]== "sphere"):
    genSphere(int(sys.argv[2]),int(sys.argv[3]),int(sys.argv[4]))
elif (sys.argv[1]== "tore"):
    genTor(int(sys.argv[2]),int(sys.argv[3]),int(sys.argv[4]),int(sys.argv[5]))

