# Solution au devoir 1
La version de python utilisée est python 3.6
## Auteur

Eric CERVEAU CERE26039805

## Solution à la question 1

Le fichier q1.py contient la réponse à la question n°1. Il est possible de la tester à l'aide de la commande proposée dans l'énnoncée : python -m doctest q1.py

## Solution à la question 2
Le fichier q2.py contient les réponses  aux différentes question 2.1, 2.2 et 2.3
2.1 on a créer une classe pour representer un cercle, une box, et une scene. La scene contient une liste d'objet qui peuvent être une box ou un cercle. 
Il est possible de tester le bon déroulement du script pour la question 2.1 à l'aide de la commande python :
    q2.py exemple/scene.json
où le deuxieme argument est le fichier json contenant les différentes informations de la scene.

2.2 On utilise la classe bibliothèque pillow pour créer l'image demandée, il est possible de tester le script pour répondre à la question 2.2 à l'aide de la commande 
python q2.py exemple/scene.json scene.png,
où le deuxieme argument est le nom du fichier que vous voulez générer suivi de l'extension .png

2.3

On a tenter de modéliser le rayon de lumière à l'aide d'une classe Lumiere, cette partie ne fonctionne pas. Le code a donc été placer en commentaire pour nous permettre de lancer les scritp pour répondre à q2.1 et Q2.2 sans erreur.
Il est tout de même possible de tester la partie concercant la lumière en décommentant la partie concernée (lignes 126 à 156) et en éxécutant la commande proposée dans l'énnoncé.

## Solution à la question 3

pour le cercle, il faut lancer la commande python 
q3.py sphere 5 32 16 > sphere.obj proposée dans l'énnoncée,
et python q3.py tore 5 2 32 16 > tore.obj pour le tore

/!\ le script fonctionne sous MacOS et Linux, mais produit un fichier dégénérer sous windows ( bien que le contenu semble être le même, à la distinction près qu'il comporte une précision plus grande
pour les décimales), une tentative de fix a été de limité à 12 le nombre de décimales pour chaque donnée, étant donné que certains nombre possédaient 12 décimale dans le fichier test founir dans l'énnoncé. 


## Solution à la question 4

 /!\ Sous windows, ajouter une variable dans PATH pour permettre au système d'éxécuter le script en utilisant la commande blender, tout en se trouvant dans le répertoir contenant le fichier q4.py

## Dépendances

Pillow et q1.py sont nécessaires pour executer q2.py, sinon toutes les librairies utilisées sont des librairies de bases dans python 3.6

## Références

LE cours d'openClassroum sur python : https://openclassrooms.com/courses/apprenez-a-programmer-en-python/qu-est-ce-que-python
https://math.stackexchange.com/questions/13261/how-to-get-a-reflection-vector
StackOVerflow : pour apprendre à utiliser les variables sytèmes.

## État du devoir

La q2.3 est incomplète ( commencée mais loin de fonctionner )
Pour la q3 : les normales sur le tore ne sont pas toutes bien positionnées.