from q1 import*
import json
import sys
from pprint import pprint
from PIL import Image,ImageDraw

class Cercle3D(object):


    def __init__(self, x, y, radius,typeO):
        r"""
        Creates an instance of 3D Cirle
        """

        self.centre = Point3D(x,y,0)
        self.radius = int(radius)
        self.type = typeO

    def isInside(self,point):
        r""" retourne vrai si le point est à l'intérieur du cercle """
        return ((point.x-self.centre.x)**2 + (point.y-self.centre.y)**2 <= self.radius**2)
    
    def giveVector(self,point):
        r""" return the 3D on which the light should reflect on"""
        vRayon = point - self.centre
        vNormal = Vector3D(-vRayon.y,vRayon.x,0)
        return vNormal



class Scene(object):


    def __init__(self, width, height):
        r"""
        Creates an instance of Scene
        """
        self.coinBasGauche =  Point3D(0,0,0)
        self.coinHautGauche =  Point3D(0,height,0)
        self.coinBasDroite =  Point3D(width,0,0)
        self.coinHautDroite =  Point3D(width,height,0)
        self.contenu =  list()
        self.width = int(width)
        self.height = int(height)
        #remove pour enlever, append pour ajouter (travaille directement sur l'objet)

class box3D(object):
    
    def __init__(self,x,y,width,height,type0):
        r"""
        creates an instace of box3D
        """
        self.width = int(width)
        self.height = int(height)
        self.centre =  Point3D(x,y,0)
        self.coinBasGauche =  Point3D(x-width/2,y-height/2,0)
        self.coinHautGauche =  Point3D(x-width/2,y+height/2,0)
        self.coinBasDroite =  Point3D(x+width/2,y-height/2,0)
        self.coinHautDroite =  Point3D(x+width/2,y+height/2,0)
        self.type = type0

    def isInside(self,point):
        r""" return true if the point is inside the box"""
        return (point.x>=self.coinBasGauche.x and point.x<self.coinBasDroite.x and  point.y>self.coinBasGauche.y and point.y <self.coinHautGauche.y)
    
    def giveVector(self,point):
        r""" return the 3D vector on which the light should reflect on"""
        vRayon = point - self.centre
        vNormal = Vector3D(-vRayon.y,vRayon.x,0)
        return vNormal
    def impact(self,Point3D,Vector3D):
        r""" return the 3D point where the light did cross the box"""


class Lumiere(object):
    def __init__(self,x0,y0,xV,yV,i):
        r"""
        creates an instace Lumiere
        """
        self.origine = Point3D(x0,y0,0)
        self.direction = Vector3D(xV,yV,0)
        self.intensite = int(i)
        self.posCourrant = self.origine
       

with open(sys.argv[1]) as json_data:   
    d = json.load(json_data)
    json_data.close()
    scene = Scene(d['width'],d['height'])
    scene.contenu
    objets = d['objects']
    for obj in objets:
        if (obj['type']=="circle"):
            scene.contenu.append(Cercle3D(obj['center'][0],obj['center'][1],obj['radius'],obj['type']))
        else:
            scene.contenu.append(box3D(obj['center'][0],obj['center'][1],obj['width'],obj['height'],obj['type']))
    print("Scene of dimension ", scene.width, " x ", scene.height)
    print("Containing ", len(scene.contenu)," objects : ")

    img = Image.new('RGB', (scene.width, scene.height), color = 'white')
    draw = ImageDraw.Draw(img)
    

    for obj in scene.contenu:
        if (obj.type == "box"):
            print("\t A box of width ", obj.width, " and height ", obj.height, " centered in ", obj.centre)
            points=[]
            #points.append((obj.coinBasDroite.x,obj.coinBasDroite.y))
            points.append((obj.coinHautDroite.x,obj.coinHautDroite.y))
            #points.append((obj.coinHautGauche.x,obj.coinHautGauche.y))
            points.append((obj.coinBasGauche.x,obj.coinBasGauche.y))
            draw.rectangle(points,fill=128)
        else :
            if (obj.type == "circle"):
                print("\t A cirle of radius", obj.radius, " centered in ", obj.centre)
                i=0
                j=0
                while (i < scene.width):
                    while (j < scene.height):                        
                        if ((i-obj.centre.x)**2 + (j-obj.centre.y)**2 <= obj.radius**2):
                            
                            draw.point((i,j),fill=128)
                        j += 1
                    i += 1
                    j=0
    #infoLulu = sys.argv[3].split(",")
    #lulu = Lumiere(infoLulu[0],infoLulu[1],infoLulu[2],infoLulu[3],infoLulu[4])

    #paramétrer la droite
    #i=0
    #while (i<lulu.intensite) :
    #    print(i)
    #    t=0
    #   
    #    while (t<1000) :
    #        
    #        currentX = int(lulu.posCourrant.x + t*lulu.direction.x)
    #        currentY = int(lulu.posCourrant.y + t*lulu.direction.y)
    #        #print("X : ", currentX, " Y : ", currentY)
    #        if (currentX<0 or currentX>scene.width or currentY<0 or currentY>scene.height) :
    #            print("X : ", currentX, " Y : ", currentY)
    #            reflectOn = Vector3D(0,0,0)
    #            if (currentX<=0 or currentX>=scene.width) :
    #                reflectOn = Vector3D(scene.width,0,0)
    #            if (currentY<=0 or currentY>=scene.height) :
    #                reflectOn = Vector3D(0,scene.height,0)
    #                
    #            points = [(lulu.origine.x,lulu.origine.y),(currentX,currentY)]
    #            draw.line(points,fill=80)
    #            nvlleDir = lulu.direction.reflect(reflectOn)
    #            lulu.direction = nvlleDir
    #            lulu.origine = Point3D(currentX,currentY,0)            
    #            i+=1
    #            break
            #print("nouvelle origine",lulu.origine)
    #        t+=0.1
    
    if(len(sys.argv)==3):
        img.save(sys.argv[2])
    
       