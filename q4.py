import bpy
import random
import sys
import json
import os
from math import pi

etoiles_objets = []
planete_objets = []

class Etoile:

    def __init__(self, nom, couleur, bpy_instance):
        self.nom = nom
        self.couleur = couleur
        self.bpy_instance = bpy_instance

    def __repr__(self):
        return self.nom + ":\n\t-couleur:" + str(self.couleur) + "\n\t-blender instance:" + str(self.bpy_instance)

class Planete:

    def __init__(self, nom, couleur, periode, bpy_instance):
        self.nom = nom
        self.couleur = couleur
        self.periode = periode
        self.bpy_instance = bpy_instance
        self.objRotation = self.creerAxeRotation()
        self.ajouterAnimation()

    def creerAxeRotation(self):
        empty = bpy.data.objects.new("empty", None)
        bpy.context.scene.objects.link(empty)
        self.bpy_instance.parent = empty
        return empty

    def ajouterAnimation(self):
        bpy.context.scene.frame_set(0)
        self.objRotation.rotation_euler = (0,0,0)
        self.objRotation.keyframe_insert(data_path="rotation_euler", index=-1)

        for fcurve in self.objRotation.animation_data.action.fcurves:
            kf = fcurve.keyframe_points[-1]
            kf.interpolation = 'LINEAR'
        
        bpy.context.scene.frame_set(self.periode)
        self.objRotation.rotation_euler = (0,0,2*pi)
        self.objRotation.keyframe_insert(data_path="rotation_euler", index=-1)

        for fcurve in self.objRotation.animation_data.action.fcurves:
            kf = fcurve.keyframe_points[-1]
            kf.interpolation = 'LINEAR'
            fcurve.modifiers.new('CYCLES')
  

def genEtoile(nom, rayon, couleur):
    genSphere(rayon, 0.0)
    etoiles_objets.append(Etoile(nom, couleur, bpy.context.active_object)) 
    addMateriau(bpy.context.active_object, couleur, nom)

def genPlan(nom, rayon, dist_etoile, periode, couleur):
    genSphere(rayon, dist_etoile)
    planete_objets.append(Planete(nom, couleur, periode, bpy.context.active_object))
    addMateriau(bpy.context.active_object, couleur, nom)

def genSphere(rayon, pos_x):
    print("\n" + str(type(rayon)) + "\t" + str(rayon) + "\n")
    bpy.ops.mesh.primitive_uv_sphere_add(size = rayon, location=(pos_x,0.0,0.0))

def addMateriau(objet, couleur, nom):
    bpy.context.scene.objects.active = objet

    mat = bpy.data.materials.get(nom)
    if mat is None:
        mat = bpy.data.materials.new(name = nom)

    mat.diffuse_color = couleur
    objet.data.materials[0] if objet.data.materials else objet.data.materials.append(mat)

def rendu(nom):
    
    s = bpy.data.scenes[0]
    s.render.image_settings.file_format = 'FFMPEG'
    s.render.ffmpeg.codec = 'H264'
    s.render.ffmpeg.format = "OGG"
    s.frame_end = 250
    s.render.fps = 30
    bpy.context.scene.render.filepath = os.getcwd()+"\\" + nom
    print(bpy.context.scene.render.filepath)
    bpy.ops.render.render(animation=True)

def main(argv):
    if (len(argv) < 3): 
        return -1
    bpy.ops.wm.open_mainfile(filepath=argv[1])
    bpy.data.cameras['Camera'].sensor_width = 60
    j = json.loads(open(argv[0]).read())
    genEtoile(j['star']['name'], j['star']['radius'], j['star']['color'])

    for planete in j['planets']:
        genPlan(planete['name'], planete['radius'], planete['distance-from-star'], planete['period'], planete['color'])
    
    rendu(argv[2])

    print("fichier sauvegarder dans :"+ os.getcwd()+"\\" + argv[2])
    bpy.ops.wm.save_as_mainfile(filepath='init2.blend')

if __name__=='__main__':
    main(sys.argv[sys.argv.index("--") + 1:])
